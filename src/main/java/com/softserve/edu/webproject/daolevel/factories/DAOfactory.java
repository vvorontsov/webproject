package com.softserve.edu.webproject.daolevel.factories;

import com.softserve.edu.webproject.daolevel.DAOelements.*;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

/**
 * Class with factories of DAO objects.
 */
@Component
public class DAOfactory {
    private static CategoryDAO categoryDAO = null;
    private static ClientDAO clientDAO = null;
    private static CountryDAO countryDAO = null;
    private static GoodDAO goodDAO = null;
    private static OrderDAO orderDAO = null;
    private static ProviderDAO providerDAO = null;
    private static SpetializationDAO spetializationDAO = null;

    private DAOfactory() {
    }

    public static DAOfactory getDAOfactory(){
        return new DAOfactory();
    }

    public CategoryDAO getCategory() {
        if (categoryDAO == null) {
            categoryDAO = new CategoryDAO();
        }
        return categoryDAO;
    }

    public ClientDAO getClient() {
        if (clientDAO == null) {
            clientDAO = new ClientDAO();
        }
        return clientDAO;
    }

    public CountryDAO getCountry() {
        if (countryDAO == null) {
            countryDAO = new CountryDAO();
        }
        return countryDAO;
    }

    public GoodDAO getGood() {
        if (goodDAO == null) {
            goodDAO = new GoodDAO();
        }
        return goodDAO;
    }

    public OrderDAO getOrder() {
        if (orderDAO == null) {
            orderDAO = new OrderDAO();
        }
        return orderDAO;
    }

    public ProviderDAO getProvider() {
        if (providerDAO == null) {
            providerDAO = new ProviderDAO();
        }
        return providerDAO;
    }

    public SpetializationDAO getSpetialization() {
        if (spetializationDAO == null) {
            spetializationDAO = new SpetializationDAO();
        }
        return spetializationDAO;
    }
}

