package com.softserve.edu.webproject.daolevel.DAOelements;


import com.softserve.edu.webproject.daolevel.ElementDAOimpl;
import com.softserve.edu.webproject.entity.Spetialization;
import org.springframework.stereotype.Repository;

@Repository
public class SpetializationDAO extends ElementDAOimpl<Spetialization> {

    public SpetializationDAO() {super(Spetialization.class);
    }
}
