package com.softserve.edu.webproject.service;

import java.util.List;

public interface ServiceCRUDinterface<E> {
    public void insert(E entity);

    public List<E> selectAll();

    public E selectById(Integer id);

    public void update(E entity);

    public void delete(E entity);
}
