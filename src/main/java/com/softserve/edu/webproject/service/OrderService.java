package com.softserve.edu.webproject.service;

import com.softserve.edu.webproject.daolevel.factories.DAOfactory;
import com.softserve.edu.webproject.entity.Order;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class OrderService {


    @Autowired
    private DAOfactory daOfactory;

    @Autowired
    private SessionFactory sessionFactory;

    public OrderService() {}

    @Transactional
    public void insert(Order order) {
        daOfactory.getOrder().addElement(order, sessionFactory);
    }

    @Transactional
    public List<Order> selectAll() {
        return daOfactory.getOrder().getAllElements(sessionFactory);
    }

    @Transactional
    public Order selectById(Integer id) {
        return daOfactory.getOrder().getElementByID(id, sessionFactory);
    }

    @Transactional
    public void delete(Order order) {
        daOfactory.getOrder().deleteElement(order, sessionFactory);
    }

    @Transactional
    public void update(Order order) {
        daOfactory.getOrder().updateElement(order, sessionFactory);
    }
}
