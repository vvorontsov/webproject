package com.softserve.edu.webproject.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;


/**
 * Entity class for table orders.
 */
@Entity
@Table(name = "order")
public class Order {
    @Id
    @NotNull
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "order_code")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "clients_code")
    private Client client;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    @JoinTable(name = "list_of_goods",
    joinColumns = {@JoinColumn(name = "order_code")},
    inverseJoinColumns = {@JoinColumn(name = "good_code")})
    private Set<Good> goodSet = new HashSet<Good>();

    public Order() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public Client getClient() {
        return client;
    }

    public void setClient(Client clients) {
        this.client = clients;
    }
}
