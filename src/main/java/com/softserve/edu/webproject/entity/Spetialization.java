package com.softserve.edu.webproject.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

/**
 * Entity class foe table spetializations
 */
@Entity
@Table(name = "spetialization")
public class Spetialization {
    @Id
    @NotNull
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "code_of_spetialization")
    private Integer codeOfSpetialization;

    @NotNull
    @Column(name = "name")
    private String name;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "spetialization")
    private Set<Good> goodSet = new HashSet<Good>();

    @ManyToOne
    @JoinColumn(name = "code_of_category")
    private Category category;


    public Spetialization() {

    }

    public Integer getCodeOfSpetialization() {
        return codeOfSpetialization;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
