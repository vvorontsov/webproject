package com.softserve.edu.webproject.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

/**
 * Entity class for table categories.
 */
@Entity
@Table(name = "category")
public class Category {

    @Id
    @NotNull
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "code_of_category")
    private Integer codeOfCategory;

    @NotNull
    @Column(name = "name")
    private String name;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "category")
    private Set<Spetialization> spetializationSet = new
            HashSet<Spetialization>();

    public Category() {
    }

    public Integer getCodeOfCategory() {
        return codeOfCategory;
    }

    public void setCodeOfCategory(Integer codeOfCategory) {
        this.codeOfCategory = codeOfCategory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Spetialization> getSpetializationSet() {
        return spetializationSet;
    }

    public void setSpetializationSet(Set<Spetialization> spetializationSet) {
        this.spetializationSet = spetializationSet;
    }
}
