package com.softserve.edu.webproject.service;

import com.softserve.edu.webproject.entity.Category;
import com.softserve.edu.webproject.entity.Spetialization;

import java.util.List;

public interface SpetializationServiceIntr extends ServiceCRUDinterface<Spetialization> {
    public List<Spetialization> getListOfSpetializationsByCategory(Category
                                                                           category);
}
