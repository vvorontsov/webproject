package com.softserve.edu.webproject.daolevel.DAOelements;


import com.softserve.edu.webproject.daolevel.ElementDAOimpl;
import com.softserve.edu.webproject.entity.Category;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

/**
 * Created by Vlad on 15.06.2017.
 */
@Repository
public class CategoryDAO extends ElementDAOimpl<Category> {

    public CategoryDAO() {
        super(Category.class);
    }
}
