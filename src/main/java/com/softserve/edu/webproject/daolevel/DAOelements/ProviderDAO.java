package com.softserve.edu.webproject.daolevel.DAOelements;


import com.softserve.edu.webproject.daolevel.ElementDAOimpl;
import com.softserve.edu.webproject.entity.Provider;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ProviderDAO extends ElementDAOimpl<Provider> {

    public ProviderDAO() {
        super(Provider.class);
    }
}
