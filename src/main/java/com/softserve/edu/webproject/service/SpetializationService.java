package com.softserve.edu.webproject.service;

import com.softserve.edu.webproject.daolevel.factories.DAOfactory;
import com.softserve.edu.webproject.entity.Category;
import com.softserve.edu.webproject.entity.Spetialization;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


@Service
public class SpetializationService implements SpetializationServiceIntr {

    @Autowired
    private DAOfactory daOfactory;

    @Autowired
    private SessionFactory sessionFactory;

    public SpetializationService() {}

    @Transactional
    public List<Spetialization> getListOfSpetializationsByCategory(Category category) {
        List<Spetialization> returnList = new ArrayList<>();
        for (Spetialization spetialization : selectAll()) {
            if (spetialization.getCategory().getName().equals(category.getName())) {
                returnList.add(spetialization);
            }
        }
        return returnList;
    }

    @Transactional
    public void insert(Spetialization spetialization) {
        daOfactory.getSpetialization().addElement(spetialization, sessionFactory);
    }

    @Transactional
    public List<Spetialization> selectAll() {
        return daOfactory.getSpetialization().getAllElements(sessionFactory);
    }

    @Transactional
    public Spetialization selectById(Integer id) {
        return daOfactory.getSpetialization().getElementByID(id, sessionFactory);
    }

    @Transactional
    public void delete(Spetialization spetialization) {
        daOfactory.getSpetialization().deleteElement(spetialization, sessionFactory);
    }

    @Transactional
    public void update(Spetialization spetialization) {
        daOfactory.getSpetialization().updateElement(spetialization, sessionFactory);
    }
}
