package com.softserve.edu.webproject.model;

import com.softserve.edu.webproject.entity.Category;
import com.softserve.edu.webproject.entity.Spetialization;
import com.softserve.edu.webproject.service.SpetializationService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.LinkedList;
import java.util.List;

public class CategoryAndSpetialsList {


    private Category category;
    private List<Spetialization> spetializations;

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public List<Spetialization> getSpetializations() {
        return spetializations;
    }

    public void setSpetializations(List<Spetialization> spetializations) {
        this.spetializations = spetializations;
    }
}
