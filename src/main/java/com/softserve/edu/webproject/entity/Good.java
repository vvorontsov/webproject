package com.softserve.edu.webproject.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Entity class foe table goods.
 */
@Entity
@Table(name = "good")
public class Good {
    @Id
    @NotNull
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "good_code")
    private Integer goodCode;

    @NotNull
    @Column(name = "name")
    private String name;

    @NotNull
    @Column(name = "price")
    private Double price;

    @Column(name = "characteristics")
    private String characteristics;

    @ManyToOne()
    @JoinColumn(name = "code_of_spetialization")
    private Spetialization spetialization;

    @ManyToOne
    @JoinColumn(name = "providers_code")
    private Provider provider;


    @ManyToMany(mappedBy = "goodSet")
    private Set<Order> ordersSet = new HashSet<Order>();

    public Good() {
    }

    public Integer getGoodCode() {
        return goodCode;
    }

    public void setGoodCode(Integer goodCode) {
        this.goodCode = goodCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getCharacteristics() {
        return characteristics;
    }

    public void setCharacteristics(String characteristics) {
        this.characteristics = characteristics;
    }


    public Collection<Order> getOrders() {
        return ordersSet;
    }

    public void setOrders(Set<Order> orders) {
        this.ordersSet = orders;
    }


    public Spetialization getSpetialization() {
        return spetialization;
    }

    public void setSpetialization(Spetialization spetialization) {
        this.spetialization = spetialization;
    }


    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }
}
