package com.softserve.edu.webproject.daolevel.DAOelements;


import com.softserve.edu.webproject.daolevel.ElementDAOimpl;
import com.softserve.edu.webproject.entity.Country;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CountryDAO extends ElementDAOimpl<Country> {

    public CountryDAO() {
        super(Country.class);
    }
}
