package com.softserve.edu.webproject.service;

import com.softserve.edu.webproject.entity.Provider;
import com.softserve.edu.webproject.daolevel.factories.DAOfactory;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by Vlad on 26.06.2017.
 */
public class ProviderService {

    public ProviderService() {
    }

    @Autowired
    private DAOfactory daOfactory;


    @Autowired
    private SessionFactory sessionFactory;

    public void insert(Provider provider) {
        daOfactory.getProvider().addElement(provider, sessionFactory);
    }

    public List<Provider> selectAll() {
        return daOfactory.getProvider().getAllElements(sessionFactory);
    }

    public Provider selectById(Integer id) {
        return daOfactory.getProvider().getElementByID(id, sessionFactory);
    }

    public void delete(Provider provider) {
        daOfactory.getProvider()
                .deleteElement(provider, sessionFactory);
    }

    public void update(Provider provider) {
        daOfactory.getProvider().updateElement(provider, sessionFactory);
    }
}
