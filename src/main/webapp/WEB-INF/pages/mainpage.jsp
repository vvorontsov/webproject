<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Music Shop</title>
</head>
<body>
<h3>Main Page</h3>
<tr>
    <c:forEach items="${categories}" var="item">
        <ul class="dropdown">
            <li class="dropdown-top">
                <a class="dropdown-top" href="/">${item.category.name}</a>
                <ul class="dropdown-inside">
                    <c:forEach items="${item.spetializations}"
                               var="spec">
                        <li><a href="/">${spec.name}</a></li>
                    </c:forEach>
                </ul>
            </li>
        </ul>
    </c:forEach>
</tr>
</body>
</html>
