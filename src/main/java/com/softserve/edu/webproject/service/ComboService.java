package com.softserve.edu.webproject.service;

import com.softserve.edu.webproject.entity.Category;
import com.softserve.edu.webproject.model.CategoryAndSpetialsList;

import java.util.List;

public interface ComboService {
    public List<CategoryAndSpetialsList> getCategoryAndSpetialsList
            (List<Category> list);
}
