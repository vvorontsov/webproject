package com.softserve.edu.webproject.daolevel;

import org.hibernate.SessionFactory;

import java.util.List;

/**
 * DAO interface
 */
public interface ElementDAO<E> {

    public void addElement(E element, SessionFactory sessionFactory);

    public void updateElement(E element, SessionFactory sessionFactory);

    public E getElementByID(Integer elementId, SessionFactory sessionFactory);

    public List<E> getAllElements(SessionFactory sessionFactory);

    public void deleteElement(E element, SessionFactory sessionFactory);
}
