package com.softserve.edu.webproject.controller;

import com.softserve.edu.webproject.daolevel.factories.DAOfactory;
import com.softserve.edu.webproject.entity.Category;
import com.softserve.edu.webproject.model.CategoryAndSpetialsList;
import com.softserve.edu.webproject.service.CategoryService;
import com.softserve.edu.webproject.service.ComboService;
import com.softserve.edu.webproject.service.ServiceCRUDinterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
public class mainController {
    @Autowired
    @Qualifier("categoryService")
    private ServiceCRUDinterface categoryService;

    @Autowired
    private ComboService comboService;

    @RequestMapping("/")
    public String handleIndexGet() {
        return "index";
    }

    @RequestMapping(value = {"/mainpage"}, method = RequestMethod.GET)
    public String handleMainPageGet(Model model) {
        List<Category> listOfCategory= categoryService.selectAll();
        List<CategoryAndSpetialsList> fields = comboService
                .getCategoryAndSpetialsList(listOfCategory);
        model.addAttribute("categories", fields);
        return "mainpage";
    }

}
