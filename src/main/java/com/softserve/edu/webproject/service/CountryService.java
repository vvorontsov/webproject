package com.softserve.edu.webproject.service;

import com.softserve.edu.webproject.daolevel.factories.DAOfactory;
import com.softserve.edu.webproject.entity.Country;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CountryService implements ServiceCRUDinterface<Country> {

    @Autowired
    private DAOfactory daOfactory;

    @Autowired
    private SessionFactory sessionFactory;

    public CountryService(){}

    @Transactional
    public void insert(Country country) {
        daOfactory.getCountry().addElement(country, sessionFactory);
    }

    @Transactional
    public List<Country> selectAll() {
        return daOfactory.getCountry().getAllElements(sessionFactory);
    }

    @Transactional
    public Country selectById(Integer id) {
        return daOfactory.getCountry().getElementByID(id,sessionFactory);
    }

    @Transactional
    public void delete(Country country) {
        daOfactory.getCountry().deleteElement(country,sessionFactory);
    }

    @Transactional
    public void update(Country country) {
        daOfactory.getCountry().updateElement(country, sessionFactory);
    }
}
