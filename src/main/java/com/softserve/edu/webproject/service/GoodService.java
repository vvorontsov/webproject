package com.softserve.edu.webproject.service;

import com.softserve.edu.webproject.daolevel.factories.DAOfactory;
import com.softserve.edu.webproject.entity.Good;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.AssertFalse;
import java.util.List;

@Service
public class GoodService {

    @Autowired
    private DAOfactory daOfactory;

    private SessionFactory sessionFactory;

    public GoodService() {}

    @Transactional
    public void insert(Good good) {
        daOfactory.getGood().addElement(good, sessionFactory);
    }

    @Transactional
    public List<Good> selectAll() {
        return daOfactory.getGood().getAllElements(sessionFactory);
    }

    @Transactional
    public Good selectById(Integer id) {
        return daOfactory.getGood().getElementByID(id, sessionFactory);
    }

    @Transactional
    public void delete(Good good) {
        daOfactory.getGood().deleteElement(good, sessionFactory);
    }

    @Transactional
    public void update(Good good) {
        daOfactory.getGood().updateElement(good, sessionFactory);
    }
}
