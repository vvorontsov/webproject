package com.softserve.edu.webproject.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * entity class for table clients.
 */
@Entity
@Table(name = "client")
public class Client {
    @Id
    @NotNull
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "clients_code")
    private Integer id;

    @Column(name = "name")
    @NotNull
    private String name;

    @Column(name = "date_of_birth")
    private Date dateOfBirth;

    @Column(name = "adress")
    private String adress;

    @Column(name = "contacts")
    @NotNull
    private String contacts;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "client")
    private Set<Order> orders = new HashSet<Order>();

    public Client() {
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getContacts() {
        return contacts;
    }

    public void setContacts(String contacts) {
        this.contacts = contacts;
    }

    public Set<Order> getOrders() {
        return orders;
    }

    public void setOrders(Set<Order> orders) {
        this.orders = orders;
    }
}
