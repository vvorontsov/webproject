package com.softserve.edu.webproject.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

/**
 * Entity class foe table providers.
 */
@Entity
@Table(name = "provider")
public class Provider {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @NotNull
    @Column(name = "providers_code")
    private Integer providersCode;

    @NotNull
    @Column(name = "providers_name")
    private String providersName;

    @Column(name = "contact_persons_name")
    private String contactPersonsName;

    @NotNull
    @Column(name = "contacts")
    private String contacts;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "provider")
    private Set<Good> goodSet = new HashSet<Good>();

    @ManyToOne
    @JoinColumn(name = "country_code")
    private Country country;

    public Provider() {

    }
    public Integer getProvidersCode() {
        return providersCode;
    }

    public void setProvidersCode(Integer providersCode) {
        this.providersCode = providersCode;
    }

    public String getProvidersName() {
        return providersName;
    }

    public void setProvidersName(String providersName) {
        this.providersName = providersName;
    }

    public String getContactPersonsName() {
        return contactPersonsName;
    }

    public void setContactPersonsName(String contactPersonsName) {
        this.contactPersonsName = contactPersonsName;
    }

    public String getContacts() {
        return contacts;
    }

    public void setContacts(String contacts) {
        this.contacts = contacts;
    }


    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }
}
