package com.softserve.edu.webproject.daolevel.DAOelements;


import com.softserve.edu.webproject.daolevel.ElementDAOimpl;
import com.softserve.edu.webproject.entity.Client;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ClientDAO extends ElementDAOimpl<Client> {

    public ClientDAO() {
        super(Client.class);
    }

    /*
    @Override
    public List<Client> getAllElements(){
        Session session = null;
        List<Client> elements = new ArrayList<Client>();
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            elements = session.createNativeQuery("select * from clients",
                    Client.class).getResultList();
        } finally {
            session.close();
        }
        return elements;
    }
    */
}
