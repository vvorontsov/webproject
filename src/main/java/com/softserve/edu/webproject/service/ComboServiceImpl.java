package com.softserve.edu.webproject.service;

import com.softserve.edu.webproject.entity.Category;
import com.softserve.edu.webproject.model.CategoryAndSpetialsList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
public class ComboServiceImpl implements ComboService {

    @Autowired
    @Qualifier("spetializationService")
    private SpetializationServiceIntr spetializationService;

    public ComboServiceImpl() {
    }

    public List<CategoryAndSpetialsList> getCategoryAndSpetialsList
            (List<Category> list) {
        List<CategoryAndSpetialsList> retList = new LinkedList<>();
        for (Category category : list) {
            CategoryAndSpetialsList item = new CategoryAndSpetialsList();
            item.setCategory(category);
            item.setSpetializations(spetializationService.getListOfSpetializationsByCategory(category));
            retList.add(item);
        }
        return retList;
    }
}
