package com.softserve.edu.webproject.service;

import com.softserve.edu.webproject.daolevel.factories.DAOfactory;
import com.softserve.edu.webproject.entity.Category;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Vlad on 25.06.2017.
 */
@Service
public class CategoryService implements ServiceCRUDinterface<Category> {

    @Autowired
    private DAOfactory daOfactory;

    @Autowired
    private SessionFactory sessionFactory;

    @Transactional
    public void insert(Category category) {
        daOfactory.getCategory().addElement(category, sessionFactory);
    }

    @Transactional
    public List<Category> selectAll() {
        return daOfactory.getCategory().getAllElements(sessionFactory);
    }

    @Transactional
    public Category selectById(Integer id) {
        return daOfactory.getCategory().getElementByID(id, sessionFactory);
    }

    @Transactional
    public void delete(Category category) {
        daOfactory.getCategory().deleteElement(category, sessionFactory);
    }

    @Transactional
    public void update(Category category) {
        daOfactory.getCategory().updateElement(category, sessionFactory);
    }
}