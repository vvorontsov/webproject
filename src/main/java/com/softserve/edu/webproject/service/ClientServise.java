package com.softserve.edu.webproject.service;

import com.softserve.edu.webproject.daolevel.factories.DAOfactory;
import com.softserve.edu.webproject.entity.Client;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ClientServise implements ServiceCRUDinterface<Client> {

    @Autowired
    private DAOfactory daOfactory;

    @Autowired
    private SessionFactory sessionFactory;

    public ClientServise(){}

    @Transactional
    public void insert(String name, String contacts) {
        Client client = new Client();
        client.setName(name);
        client.setContacts(contacts);
        daOfactory.getClient().addElement(client, sessionFactory);
    }

    @Transactional
    public void insert(Client client) {
        daOfactory.getClient().addElement(client, sessionFactory);
    }

    @Transactional
    public List<Client> selectAll() {
        return daOfactory.getClient().getAllElements(sessionFactory);
    }

    @Transactional
    public Client selectById(Integer id) {
        return daOfactory.getClient().getElementByID(id, sessionFactory);
    }

    @Transactional
    public void delete(Client client) {
        daOfactory.getClient().deleteElement(client, sessionFactory);
    }

    @Transactional
    public void update(Client client) {
        daOfactory.getClient().updateElement(client,sessionFactory);
    }
}
